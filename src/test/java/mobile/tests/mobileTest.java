package mobile.tests;

import java.io.IOException;
import java.util.List;

import org.testng.annotations.Test;

import businesslogic.mobile.encash.EncashMobileMethods;
import common.mobile.mobileDriver;
import data.Testdata;
import locators.mobile.encash.EncashMobileLocators;

public class mobileTest extends mobileDriver{

	Testdata data=new Testdata();
	EncashMobileMethods mob=new EncashMobileMethods();
	
	@Test(priority=1)
	public void m_verifyLogo() throws IOException {

		String logo=null;
		String expLogo=data.expLogo;
		
		click(EncashMobileLocators.encash_mobile_acceptCookies,"Accept cookies");
		click(mob.buttonWithtext("No, I’ll explore myself"), "Click on explore myself");

		//Verify logo
		logo=getAttribute(EncashMobileLocators.encash_mobile_logo, "title", "Get title of "+EncashMobileLocators.encash_mobile_logo);
		assertEquals(expLogo, logo, "Verify logo");

	}
	
	@Test(priority=2)
	public void m_jishi() throws IOException, InterruptedException {

		//Test data
		String expLogo=data.expLogo;
		List<String> expMsgs=data.expMsgs;

		int counter=0;

		//Verify Jishi animation
		click(EncashMobileLocators.encash_mobile_acceptCookies,"Accept cookies");
		boolean jishi = isDisplayed(EncashMobileLocators.encash_mobile_JishiAnimation, "Is element displayed");
		assertTrue(jishi, "Verify jishi animation");			
		click(mob.buttonWithtext("Yes, continue"), "Click on Yes, continue");

		//Verify jishi messages
		while(counter<=4) {				
			String msg=getText(EncashMobileLocators.encash_mobile_JishiMsg, "Get text");
			System.out.println("********"+counter+"******"+expMsgs.get(counter));
			assertEquals(expMsgs.get(counter), msg, "Verify jishi messages");

			if(counter<4) {
				click(mob.buttonWithtext("Next"), "Click on Next");//Thread.sleep(500);
			}
			else if(counter==4) {
				click(mob.buttonWithtext("Close"), "Click on Close");
			}

			counter++;	
		}

		//Verify welcome page is displayed
		String logo=getAttribute(EncashMobileLocators.encash_mobile_logo, "title", "Get title");
		assertEquals(expLogo, logo, "Verify logo");

	}
	
	@Test(priority=3)
	public void m_navigationBar() throws IOException {

		//Test data
		String expLogo=data.expLogo;
		String expCompetitionsLink=data.expCompetitionsLink;
		String expOffersLink=data.expOffersLink;
		//String expHowitworksLink=data.expHowitworksLink;
		String expTxtOnScreen=data.expTxtOnScreen;

		click(EncashMobileLocators.encash_mobile_acceptCookies,"Accept cookies");
		click(mob.buttonWithtext("No, I’ll explore myself"), "Click on explore myself");

		//Verify logo
		String logo=getAttribute(EncashMobileLocators.encash_mobile_logo, "title", "Get title");
		assertEquals(expLogo, logo, "Verify logo");

		//Verify Competitions
		click(EncashMobileLocators.encash_mobile_toggleBurger,"Toggle");
		click(mob.navLink("Competitions"), "Click on Competitions");
		String CompetitionsLink=getText(EncashMobileLocators.encash_mobile_activeLinkPage, "Get text");
		assertEquals(expCompetitionsLink, CompetitionsLink, "Verify Competitions");	

		//Verify Offers
		click(EncashMobileLocators.encash_mobile_toggleBurger,"Toggle");
		click(mob.navLink("Offers"), "Click on Offers");
		String OffersLink=getText(EncashMobileLocators.encash_mobile_activeLinkPage, "Get text");
		assertEquals(expOffersLink, OffersLink, "Verify Offers");	


		//Verify How it works
		click(EncashMobileLocators.encash_mobile_toggleBurger,"Toggle");
		click(mob.navLink("How it works"), "Click on How it works");
		boolean HowitworksLink = isDisplayed(mob.txtOnScreen(expTxtOnScreen), "Is element displayed");
		assertTrue(HowitworksLink, "Verify how it works");

	}
	
}