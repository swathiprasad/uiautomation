package web.crm.tests;

import org.testng.annotations.Test;

import businesslogic.web.crm.CRMUIMethods;
import common.web.Driver;

public class CRMTest extends Driver{

	public CRMUIMethods CRMUIMethods=new CRMUIMethods();
	
	@Test(priority=1)
	public void crmUI_addCompetition() throws Exception {
		
		//Launch and Login
		CRMUIMethods.login();
		
		//Expand Encash Offers and go to Competitions
		clickAndWait(CRMUIMethods.appLink("Encash Offers"), "Expand Encash Offers");
		clickAndWait(CRMUIMethods.appLink("Competitions"),"Goto Competitions");
				
		//Logout
		CRMUIMethods.logout();
		//generateRandomNumber(10);
		
		
	}
}
