package web.encashoffers.tests;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import businesslogic.web.encash.EncashUIMethods;
import common.web.Driver;
import data.Testdata;
import locators.web.encash.EncashWebLocators;

/**
 * Unit test for simple App.
 */
public class EncashUITest extends Driver
{	
	public EncashUIMethods EncashUIMethods=new EncashUIMethods();

	static Logger logger = Logger.getLogger(EncashUITest.class);
	Testdata data=new Testdata();
	
	@Test(priority=1)
	public void encashUI_verifyLogo() throws IOException {
		EncashUIMethods.launch();

		String logo=null;
		String expLogo=data.expLogo;		

		click(EncashUIMethods.buttonWithtext("No, I’ll explore myself"), "Click on explore myself");

		//Verify logo
		logo=getAttribute(EncashWebLocators.encash_UI_logo, "title", "Get title");
		assertEquals(expLogo, logo, "Verify logo");

	}

	@Test(enabled=false)
	public void encashUI_login_logout() throws IOException {
		EncashUIMethods.launch();

		//Test data
		String email=data.email;
		String pwd=data.pwd;

		click(EncashUIMethods.buttonWithtext("No, I’ll explore myself"), "Click on explore myself");
		click(EncashUIMethods.buttonWithtext("Login"), "Click on Login");
		type(EncashUIMethods.txtfldWithPlaceholder("Email id"), email, "Type "+email);
		type(EncashUIMethods.txtfldWithPlaceholder("Password"), pwd, "Type "+pwd);
		click(EncashUIMethods.buttonWithtext("Continue"), "Click on Continue");
		clickAndWait(EncashUIMethods.buttonWithtext("Skip"), "Click on Skip");


		boolean element=isDisplayed(EncashWebLocators.encash_UI_loggedInUser, "Is element displayed");
		assertTrue(element, "Verify element");
		logout();

	}

	@Test(priority=2)
	public void encashUI_jishi() throws IOException, InterruptedException {
		EncashUIMethods.launch();
		
		logger.info("Executing the Jishi test case");
		//Test data
		String expLogo=data.expLogo;
		List<String> expMsgs=data.expMsgs;

		int counter=0;

		//Verify Jishi animation
		boolean jishi = isDisplayed(EncashWebLocators.encash_UI_JishiAnimation, "Is element displayed");
		assertTrue(jishi, "Verify jishi animation");			
		clickAndWait(EncashUIMethods.buttonWithtext("Yes, continue"), "Click on Yes, continue");

		//Verify jishi messages
		while(counter<=4) {				
			String msg=getText(EncashWebLocators.encash_UI_JishiMsg, "Get text");
			System.out.println("********"+counter+"******"+expMsgs.get(counter));
			assertEquals(expMsgs.get(counter), msg, "Verify jishi messages");

			if(counter<4) {
				click(EncashUIMethods.buttonWithtext("Next"), "Click on Next");Thread.sleep(500);
			}
			else if(counter==4) {
				clickAndWait(EncashUIMethods.buttonWithtext("Close"), "Click on Close");
			}

			counter++;	
		}

		//Verify welcome page is displayed
		String logo=getAttribute(EncashWebLocators.encash_UI_logo, "title", "Get title");
		assertEquals(expLogo, logo, "Verify logo");
		logger.info("Assert verification is done");

	}


	@Test(priority=3)
	public void encashUI_navigationBar() throws IOException {
		EncashUIMethods.launch();

		//Test data
		String expLogo=data.expLogo;
		String expCompetitionsLink=data.expCompetitionsLink;
		String expOffersLink=data.expOffersLink;
		//String expHowitworksLink=data.expHowitworksLink;
		String expTxtOnScreen=data.expTxtOnScreen;

		click(EncashUIMethods.buttonWithtext("No, I’ll explore myself"), "Click on explore myself");

		//Verify logo
		String logo=getAttribute(EncashWebLocators.encash_UI_logo, "title", "Get title");
		assertEquals(expLogo, logo, "Verify logo");

		//Verify Competitions
		click(EncashUIMethods.navLink("Competitions"), "Click on Competitions");
		String CompetitionsLink=getText(EncashWebLocators.encash_UI_activeLinkPage, "Get text");
		assertEquals(expCompetitionsLink, CompetitionsLink, "Verify Competitions");	

		//Verify Offers
		click(EncashUIMethods.navLink("Offers"), "Click on Offers");
		String OffersLink=getText(EncashWebLocators.encash_UI_activeLinkPage, "Get text");
		assertEquals(expOffersLink, OffersLink, "Verify Offers");	


		//Verify How it works
		click(EncashUIMethods.navLink("How it works"), "Click on How it works");
		boolean HowitworksLink = isDisplayed(EncashUIMethods.txtOnScreen(expTxtOnScreen), "Is element displayed");
		assertTrue(HowitworksLink, "Verify how it works");

	}

	@Test(priority=4)
	public void encashUI_Banner() throws IOException {
		EncashUIMethods.launch();

		int counter=0;

		//Test data
		List<String> expTitles=data.expTitles;

		click(EncashUIMethods.buttonWithtext("No, I’ll explore myself"), "Click on explore myself");
		scrollToView(EncashWebLocators.encash_UI_footer,"Scroll");
		scrollToView(EncashWebLocators.encash_UI_BannerNext,"Scroll");

		while(counter<=18) {
			String title = getAttribute(EncashUIMethods.bannerSub(Integer.toString(counter+1)), "title", "Get title");
			counter=expTitles.indexOf(title);
			System.out.println(counter);
			System.out.println("*****"+counter+"*******"+expTitles.get(counter));
			assertEquals(expTitles.get(counter), title, "Verify Banner");

			click(EncashWebLocators.encash_UI_BannerNext, "Click on Next");

			counter++;
		}

	}

}