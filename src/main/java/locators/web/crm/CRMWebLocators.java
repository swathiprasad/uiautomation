package locators.web.crm;

public class CRMWebLocators {
	
	//Login & Logout
	public static final String encash_crm_username="//input[@formcontrolname='userId']";
	public static final String encash_crm_pwd="//input[@formcontrolname='password']";
	public static final String encash_crm_login="//span[contains(text(),'LOGIN')]";
	public static final String encash_crm_userIcon="//button[contains(@class,'user-button')]";
	public static final String encash_crm_myProfile="//span[contains(text(),'My Profile')]";
	public static final String encash_crm_logout="//span[contains(text(),'Logout')]";
	
	//Links
	public static final String encash_ui_applicationLink="//span[contains(text(),'toReplaceLinkName')]"; 
	//Application navbar links, dropdown options, DISABLE ALL CAMPAIGNS, SAVE, Add Client, Add Category 
	
	public static final String encash_crm_productLink="//p[contains(text(),'toReplaceLinkName')]"; //Client/Competition/Offer names	
	public static final String encash_crm_tabLink="//div[contains(text(),'toReplaceLinkName')]"; //Basic Info, Questions, Traffic
	
	//Txtfld & textarea
	public static final String encash_crm_txtfldWithPlaceholder="//input[contains(@placeholder,'toReplacePlaceholder')]";//Name, Transfer URL, Search...
	public static final String encash_crm_txtareaWithPlaceholder="//textarea[contains(@placeholder,'toReplacePlaceholder')]";//Description
	//p -> Description Text area in an Offer (Basic Info)
		
	//Dropdown	
	public static final String encash_crm_dropdown="(//mat-select[@placeholder='toReplaceDropdownName'])";//Location, Category, Locations, Select questionnaire->append with [index]
	
	//Specific
	public static final String encash_crm_isCoreg="//span[contains(text(),'Is Co-Reg?')]/../..//input[@role='switch']";
	public static final String encash_crm_isActive="//span[contains(text(),'Is Active?')]/../..//input[@role='switch']";
	public static final String encash_crm_isHighValue="//span[contains(text(),'Is HighValue?')]/../..//input[@role='switch']";
	public static final String encash_crm_more="//mat-icon[contains(text(),'more_vert')]"; //3 verticals dots
	public static final String encash_crm_burgerMenu="//button[contains(@class,'navbar-toggle-button')]'"; //3 vertical dashes
	
	public static final String encash_crm_delete="/../..//mat-icon[contains(text(),'delete')]"; //append after 'encash_crm_productLink'
	public static final String encash_crm_add="/../..//mat-icon[contains(text(),'add')]"; //append after 'encash_crm_productLink'
	public static final String encash_crm_copy="/../..//mat-icon[contains(text(),'file_copy')]"; //append after 'encash_crm_productLink'
	
	public static final String encash_crm_imageUpload="//label[contains(text(),'Square')]/..//ssn-image-upload";
	
	public static final String encash_crm_endDate="//input[@formcontrolname='productEndDateTime']";
	
	
	//Pagination
	public static final String encash_crm_itemsPerPage="//*[contains(@class,'mat-paginator-page-size-select')]";
	public static final String encash_crm_pageRange="//*[contains(@class,'mat-paginator-range-actions')]";
	public static final String encash_crm_prevPage="//*[contains(@class,'mat-paginator-navigation-previous')]";
	public static final String encash_crm_nextPage="//*[contains(@class,'mat-paginator-navigation-next')]";
	
	//### Pending ###
	
}