package locators.web.encash;

public class EncashWebLocators {

	//Property file path
	public static final String projectVariables="/src/main/resources/ProjectVariable.properties";

	//login, logout
	public static final String encash_UI_otp="//body//input[number]";
	public static final String encash_UI_loggedInUser=" //div[@class='loggedin ng-star-inserted']//span";
	public static final String encash_UI_logout="//button[contains(text(),'Logout')]";
	public static final String encash_UI_menu="//span[@class='mat-menu-trigger']//img";

	//Images
	public static final String encash_UI_logo="//a[@class='navbar-brand pl-2']//img";
	public static final String encash_UI_JishiAnimation="//img[contains(@alt,'Jishi')]";
	public static final String encash_UI_profileImage="//div[@class='cdk-overlay-container']//li[toReplaceProfileNumber]";

	//Button
	public static final String encash_UI_buttonWithText="//button[contains(text(),'toReplaceButtonText')]"; 
	//Continue, Login, Register, Cancel, "Yes, continue", Next, Skip, Verify, No, I’ll explore myself, Find Address, Save

	//Textfield	
	public static final String encash_UI_txtfldWithPlaceholder="//input[@placeholder='toReplacePlacholder']"; //Email id, Password, mobile number
	//First name, last name, email, Create pwd, confirm pwd, Postal code

	//Text
	public static final String encash_UI_txtOnScreen="//h1[contains(text(),'toReplaceText')]"; //No luck by chance Chance by you
	public static final String encash_UI_JishiMsg="//div[@class='section-info']/p";

	//Link
	public static final String encash_UI_SpanLink="//span[contains(text(),'toReplaceLinkText')]/..";//Apple iPhone 11 Pro Max (256GB) - Space Grey - all competition and offer names, Done (after competition is completed)
	public static final String encash_UI_answers="//div[contains(text(),'toReplaceAnswer')]"; //Answers to questions, Male/Female gender selection
	public static final String encash_UI_normalLink="//a[contains(text(),'toReplaceLinkText')]"; // Play now, Next, Participate, About us, Contact us
	public static final String encash_UI_navLink="//a[@class='nav-link'][contains(text(),'toReplaceLinkText')]"; //Competitions, Offers, How it works
	public static final String encash_UI_activeLinkPage="//a[@class='active ng-star-inserted']"; //Home > Competitions

	//Dropdown
	public static final String encash_UI_selectBox="//select[@id='toReplaceDropdownID']"; //title, day, month, year, address

	//Containers
	public static final String encash_UI_container="//p[contains(text(),'toReplaceContainerName')]/../../../../..";// E-com, Encash Group, Retail, Travel, Utility

	//Banner
	public static final String encash_UI_BannerMain="//div[@class='desktop-carousel ng-star-inserted']//div[@class='owl-stage-outer ng-star-inserted']/owl-stage/div/div/div"; //has 19 matches (banners)
	public static final String encash_UI_BannerSub="//div[@class='desktop-carousel ng-star-inserted']//div[@class='owl-stage-outer ng-star-inserted']/owl-stage/div/div/div[toReplaceBannerNumber]/img"; 
	public static final String encash_UI_BannerNext="//div[@class='desktop-carousel ng-star-inserted']//div[@class='owl-next']//img";
	//Specific
	public static final String encash_UI_levelCompletion="//strong[@class='completion-status']";	
	public static final String encash_UI_footer="//footer[@class='wow slideInUp']";
	public static final String encash_UI_acceptCookies="//a[@id='CybotCookiebotDialogBodyLevelButtonAccept']";
	public static final String encash_UI_enterWithoutReceivingMarketingmessages="//b[contains(text(),' To enter without receiving marketing messages')]/a";

	//User preferences
	public static final String encash_UI_editCommunicationPreferences="//span[contains(@title,'Edit communication preferences')]";
	public static final String encash_UI_editCommunicationProfile="//span[contains(@title,'Edit profile')]";
	public static final String encash_UI_changePassword="//span[contains(@title,'Change password')]";

}
