package locators.mobile.encash;

public class EncashMobileLocators {	
	
	//login, logout
	public static final String encash_mobile_otp="//body//input[number]";
	public static final String encash_mobile_loggedInUser=" //div[@class='loggedin ng-star-inserted']//span";
	public static final String encash_mobile_logout="//a[contains(text(),'Log out')]"; //mobile specific
	
	//Images
	public static final String encash_mobile_logo="//a[@class='navbar-brand pl-2']//img";
	public static final String encash_mobile_JishiAnimation="//img[contains(@alt,'Jishi')]";
	public static final String encash_mobile_profileImage="//div[@class='cdk-overlay-container']//li[toReplaceProfileNumber]";

	//Button
	public static final String encash_mobile_buttonWithText="//button[contains(text(),'toReplaceButtonText')]"; 
	//Continue, Login, Register, Cancel, "Yes, continue", Next, Skip, Verify, No, I’ll explore myself, Find Address, Save
	
	//Textfield	
	public static final String encash_mobile_txtfldWithPlaceholder="//input[@placeholder='toReplacePlacholder']"; //Email id, Password, mobile number
	//First name, last name, email, Create pwd, confirm pwd, Postal code
	
	//Text
	public static final String encash_mobile_txtOnScreen="//h1[contains(text(),'toReplaceText')]"; //No luck by chance Chance by you
	public static final String encash_mobile_JishiMsg="//div[@class='section-info']/p";
	
	//Link
	public static final String encash_mobile_SpanLink="//span[contains(text(),'toReplaceLinkText')]/..";//Apple iPhone 11 Pro Max (256GB) - Space Grey - all competition and offer names, Done (after competition is completed)
	public static final String encash_mobile_answers="//div[contains(text(),'toReplaceAnswer')]"; //Answers to questions	
	public static final String encash_mobile_normalLink="//a[contains(text(),'toReplaceLinkText')]"; // Play now, Next, Participate,About us, Contact us
	public static final String encash_mobile_navLink="//ul[@class='pl-0']//a[contains(text(),'toReplaceLinkText')]"; //Competitions, Offers, How it works //mobile specific
	public static final String encash_mobile_activeLinkPage="//a[@class='active ng-star-inserted']"; //Home > Competitions
	public static final String encash_mobile_toggleBurger="//a[@class='navbar-toggler']//img"; //mobile specific
			
	//Dropdown
	public static final String encash_mobile_selectBox="//select[@id='toReplaceDropdownID']"; //title, day, month, year, address
	
	//Containers
	public static final String encash_mobile_container="//p[contains(text(),'toReplaceContainerName')]/../../../../..";// E-com, Encash Group, Retail, Travel, Utility
	
	//Banner
	public static final String encash_mobile_BannerMain="//div[@class='desktop-carousel ng-star-inserted']//div[@class='owl-stage-outer ng-star-inserted']/owl-stage/div/div/div"; //has 19 matches (banners)
	public static final String encash_mobile_BannerSub="//div[@class='desktop-carousel ng-star-inserted']//div[@class='owl-stage-outer ng-star-inserted']/owl-stage/div/div/div[toReplaceBannerNumber]/img"; 
		
	//Specific
	public static final String encash_mobile_levelCompletion="//strong[@class='completion-status']";	
	public static final String encash_mobile_footer="//footer[@class='wow slideInUp']";
	public static final String encash_mobile_acceptCookies="//a[@id='CybotCookiebotDialogBodyLevelButtonAccept']";
	public static final String encash_mobile_enterWithoutReceivingMarketingmessages="//b[contains(text(),' To enter without receiving marketing messages')]/a";
		
	//User preferences
	public static final String encash_mobile_editCommunicationPreferences="//span[contains(@title,'Edit communication preferences')]";
	public static final String encash_mobile_editCommunicationProfile="//span[contains(@title,'Edit profile')]";
	public static final String encash_mobile_changePassword="//span[contains(@title,'Change password')]";
		
}