package businesslogic.mobile.encash;

import java.util.Calendar;

import locators.mobile.encash.EncashMobileLocators;

public class EncashMobileMethods {

	public String buttonWithtext(String buttonText) {
		String locator=EncashMobileLocators.encash_mobile_buttonWithText;
		return locator.replaceAll("toReplaceButtonText", buttonText);
	}

	public String txtfldWithPlaceholder(String placeholder) {
		String locator=EncashMobileLocators.encash_mobile_txtfldWithPlaceholder;
		return locator.replaceAll("toReplacePlacholder", placeholder);
	}

	public String spanLink(String linkText) {
		String locator=EncashMobileLocators.encash_mobile_SpanLink;
		return locator.replaceAll("toReplaceLinkText", linkText);
	}

	public String normalLink(String linkText) {
		String locator=EncashMobileLocators.encash_mobile_normalLink;
		return locator.replaceAll("toReplaceLinkText", linkText);
	}

	public String navLink(String linkText) {
		String locator=EncashMobileLocators.encash_mobile_navLink;
		return locator.replaceAll("toReplaceLinkText", linkText);
	}

	public String txtOnScreen(String text) {
		String locator=EncashMobileLocators.encash_mobile_txtOnScreen;
		return locator.replaceAll("toReplaceText", text);
	}

	public String bannerSub(String bannerNumber) {
		String locator=EncashMobileLocators.encash_mobile_BannerSub;
		return locator.replaceAll("toReplaceBannerNumber", bannerNumber);
	}

	public String profileImage(String profileNumber) {
		String locator=EncashMobileLocators.encash_mobile_profileImage;
		return locator.replaceAll("toReplaceProfileNumber", profileNumber);
	}
	
	public String answers(String answer) {
		String locator=EncashMobileLocators.encash_mobile_answers;
		return locator.replaceAll("toReplaceAnswer", answer);
	}
	
	public String selectBox(String DropdownID) {
		String locator=EncashMobileLocators.encash_mobile_selectBox;
		return locator.replaceAll("toReplaceDropdownID", DropdownID);
	}
	
	public String container(String containerName) {
		String locator=EncashMobileLocators.encash_mobile_container;
		return locator.replaceAll("toReplaceContainerName", containerName);
	}
	
}
