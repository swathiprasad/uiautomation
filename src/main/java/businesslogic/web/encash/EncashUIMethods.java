package businesslogic.web.encash;

import java.util.concurrent.TimeUnit;

import common.web.Driver;
import locators.web.encash.EncashWebLocators;

public class EncashUIMethods extends Driver
{

	Driver dr=new Driver();
	
	public String buttonWithtext(String buttonText) {
		String locator=EncashWebLocators.encash_UI_buttonWithText;
		return locator.replaceAll("toReplaceButtonText", buttonText);
	}

	public String txtfldWithPlaceholder(String placeholder) {
		String locator=EncashWebLocators.encash_UI_txtfldWithPlaceholder;
		return locator.replaceAll("toReplacePlacholder", placeholder);
	}

	public String spanLink(String linkText) {
		String locator=EncashWebLocators.encash_UI_SpanLink;
		return locator.replaceAll("toReplaceLinkText", linkText);
	}

	public String normalLink(String linkText) {
		String locator=EncashWebLocators.encash_UI_normalLink;
		return locator.replaceAll("toReplaceLinkText", linkText);
	}

	public String navLink(String linkText) {
		String locator=EncashWebLocators.encash_UI_navLink;
		return locator.replaceAll("toReplaceLinkText", linkText);
	}

	public String txtOnScreen(String text) {
		String locator=EncashWebLocators.encash_UI_txtOnScreen;
		return locator.replaceAll("toReplaceText", text);
	}

	public String bannerSub(String bannerNumber) {
		String locator=EncashWebLocators.encash_UI_BannerSub;
		return locator.replaceAll("toReplaceBannerNumber", bannerNumber);
	}

	public String profileImage(String profileNumber) {
		String locator=EncashWebLocators.encash_UI_profileImage;
		return locator.replaceAll("toReplaceProfileNumber", profileNumber);
	}

	public String answers(String answer) {
		String locator=EncashWebLocators.encash_UI_answers;
		return locator.replaceAll("toReplaceAnswer", answer);
	}

	public String selectBox(String DropdownID) {
		String locator=EncashWebLocators.encash_UI_selectBox;
		return locator.replaceAll("toReplaceDropdownID", DropdownID);
	}

	public String container(String containerName) {
		String locator=EncashWebLocators.encash_UI_container;
		return locator.replaceAll("toReplaceContainerName", containerName);
	}

	public void launch() {
		String url=dr.PROCONFIG.getProperty("stagingUrl");		
		driver.get(url);try {
			Thread.sleep(5000);

			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();

			driver.navigate().refresh();
			Thread.sleep(15000);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}