package businesslogic.web.crm;

import common.web.Driver;
import locators.web.crm.CRMWebLocators;

public class CRMUIMethods extends Driver{


	public String appLink(String linkName) {
		String locator=CRMWebLocators.encash_ui_applicationLink;
		return locator.replaceAll("toReplaceLinkName", linkName);
	}

	public String productLink(String linkName) {
		String locator=CRMWebLocators.encash_crm_productLink;
		return locator.replaceAll("toReplaceLinkName", linkName);
	}

	public String tabLink(String linkName) {
		String locator=CRMWebLocators.encash_crm_tabLink;
		return locator.replaceAll("toReplaceLinkName", linkName);
	}	

	public String txtFld(String placeholder) {
		String locator=CRMWebLocators.encash_crm_txtfldWithPlaceholder;
		return locator.replaceAll("toReplacePlaceholder", placeholder);
	}

	public String textarea(String placeholder) {
		String locator=CRMWebLocators.encash_crm_txtareaWithPlaceholder;
		return locator.replaceAll("toReplacePlaceholder", placeholder);
	}

	public String dropdown(String dropdownName) {
		String locator=CRMWebLocators.encash_crm_dropdown;
		return locator.replaceAll("toReplaceDropdownName", dropdownName);
	}

	public void login() throws Exception {
		String crmURL=PROCONFIG.getProperty("crmURL");
		String crmUsername=PROCONFIG.getProperty("crmUsername");
		String crmPwd=decryptPwd(PROCONFIG.getProperty("crmPwd"));

		try {
			driver.get(crmURL);
			typeAndWait(CRMWebLocators.encash_crm_username, crmUsername, "Type username "+crmUsername);
			typeAndWait(CRMWebLocators.encash_crm_pwd, crmPwd, "Type pwd "+crmPwd);
			clickAndWait(CRMWebLocators.encash_crm_login, "Login");
			
			waitForElement(30, appLink("Encash Offers"), "Wait for Admin UI");
			//Thread.sleep(8000);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void logout() {
		try {
			clickAndWait(CRMWebLocators.encash_crm_userIcon,"Click on user icon");
			clickAndWait(CRMWebLocators.encash_crm_logout, "Logout");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}