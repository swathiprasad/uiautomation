package data;

import java.util.LinkedList;
import java.util.List;

public class Testdata {
	public String expLogo;
	public String email;
	public String pwd;
	public List<String> expMsgs=new LinkedList<String>();
	public String expCompetitionsLink;
	public String expOffersLink;
	public String expHowitworksLink;
	public String expTxtOnScreen;
	public List<String> expTitles=new LinkedList<String>();
		
	public Testdata() {	
				
		expLogo="Encash Offers Logo";

		email="gmail@gmail.com";
		pwd="password123";
		
		expMsgs.add("No luck by chance here. Play and win great products for yourself");		
		expMsgs.add("We have changed the way competitions run");	
		expMsgs.add("Browse the large catalogue");
		expMsgs.add("Look out for the competition winners in the leader board");
		expMsgs.add("Every week, highest scorer wins additional £50 gift voucher");

		expCompetitionsLink="Competitions";
		expOffersLink="Offers";
		expHowitworksLink="How it works";
		expTxtOnScreen="No luck by chance Chance by you";

		expTitles.add("Four Countries Safari");
		expTitles.add("A Taste of Jordan");
		expTitles.add("Fitbit Versa 2 Smart Watch");
		expTitles.add("Acer Nitro 5 AN515-52 Gaming Notebook");
		expTitles.add("Ethiopian Odyssey");
		expTitles.add("Samsung Galaxy S10 Smartphone");
		expTitles.add("Bosch Indego 400 Connect");
		expTitles.add("Bosch Series 8 ActiveOxygen WAW28750GB Washing Machine");
		expTitles.add("Falkland Islands Holidays");
		expTitles.add("Sony PlayStation 4 Pro");
		expTitles.add("Apple iPhone 11 Pro Max (256GB) - Space Grey");
		expTitles.add("Macbook Pro");
		expTitles.add("Apple Watch Series 5");
		expTitles.add("Essential Japan");
		expTitles.add("Amazon Gift Card");
		expTitles.add("Galaxy Fold 5G");
		expTitles.add("Dyson Cyclone V10 Cordless Vacuum Cleaner");
		expTitles.add("Best of Botswana");
		expTitles.add("Visit Vatican Museum, Sistine Chapel and St. Peter's Basilica");	
	}	
}
