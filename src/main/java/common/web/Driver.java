package common.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.Key;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.Status;
import com.google.common.io.Files;

import io.github.bonigarcia.wdm.WebDriverManager;
import locators.web.encash.EncashWebLocators;
import utils.ExtentReport;

public class Driver {
	static Logger logger = Logger.getLogger(Driver.class);
	public static WebDriver driver;

	public static Logger APPLICATION_LOGS = null;
	public static Properties CONFIG = null;
	public static Properties PROCONFIG=null;
	public static File f;
	public static ExtentReport test;
	public String foldername;


	public String getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		int day = cal.get(Calendar.DATE);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		String datevalue=day+"/"+month+"/"+year;
		datevalue=datevalue.replaceAll("/", "-");
		return datevalue;
	}

	/**
	 * Gets the current time.
	 *
	 * @return the current time
	 */
	public String getCurrentTime() {
		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		String timevalue=hour+"-"+minute+"-"+second;
		return timevalue;
	}

	@BeforeSuite
	/**
	 * Configure log4j.properties
	 */
	public void configurationRead() {
		PropertyConfigurator.configure(System.getProperty("user.dir")+File.separator+"src/main/resources"+File.separator+"log4j.properties");

		String date=getCurrentDate();
		String time=getCurrentTime();
		f=new File(System.getProperty("user.dir")+File.separator+"TestResults"+File.separator+date+"_"+time);
		f.mkdir();
		//	report = new ExtentReports(f.toString()+File.separator+"ExtentReport.html",true);
		test = new ExtentReport(f.toString()+File.separator+"ExtentReport.html") ;
		PROCONFIG = initProjectVar(EncashWebLocators.projectVariables);
		// test = new ExtentReport() ;
	}


	@BeforeMethod
	/**
	 * Browser initialization and launch URL
	 * @param m - current method name
	 * @throws Exception
	 */
	public void init(Method m) throws Exception {
		ChromeOptions chromeOptions = new ChromeOptions();
		FirefoxOptions firefoxOptions = new FirefoxOptions();
		///test=report.startTest(m.getName());
		logger.info("creating the extended test case ");
		test.CreateTest(m.getName());
		logger.info("Example for creating ");

		//Browser setup
		String browser=PROCONFIG.getProperty("browser");
		String runHeadless=PROCONFIG.getProperty("runHeadless");

		if(browser.equalsIgnoreCase("Chrome")) {
			WebDriverManager.chromedriver().setup();
			if (runHeadless.equalsIgnoreCase("true"))				
				chromeOptions.setHeadless(true);			
			driver=new ChromeDriver(chromeOptions);
		}

		else if(browser.equalsIgnoreCase("Firefox")) {
			WebDriverManager.firefoxdriver().setup();
			if (runHeadless.equalsIgnoreCase("true"))				
				firefoxOptions.setHeadless(true);			
			driver=new FirefoxDriver(firefoxOptions);
		}

		else if(browser.equalsIgnoreCase("Edge")) {
			WebDriverManager.edgedriver().setup();
			driver=new EdgeDriver();
		}

		else if(browser.equalsIgnoreCase("IE")) {
			WebDriverManager.iedriver().setup();
			driver=new InternetExplorerDriver();
		}
		else if(browser.equalsIgnoreCase("Safari")) {
			driver=new SafariDriver();
		}
		else
			System.out.println("Unsupported browser!");		

		driver.manage().window().maximize();
	}	



	/**
	 * Init projectVariables
	 * @param filePath - path to ProjectVariable.properties
	 * @return
	 */
	public Properties initProjectVar(String filePath) {
		APPLICATION_LOGS = Logger.getLogger("devpinoyLogger");
		PROCONFIG = new Properties();
		try {
			FileInputStream fs = new FileInputStream(System.getProperty("user.dir") + File.separator + filePath);
			PROCONFIG.load(fs);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//	}
		return PROCONFIG;
	}

	/**
	 * logout
	 */
	public void logout() {
		driver.findElement(By.xpath(EncashWebLocators.encash_UI_menu)).click();
		driver.findElement(By.xpath(EncashWebLocators.encash_UI_logout)).click();
	}

	//WebDriver methods
	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	public void click(String locator,String desc) throws IOException {
		try {
			driver.findElement(By.xpath(locator)).click();
			test.WriteLog(Status.PASS, "Click - "+desc);

		}

		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("Click failed", error);
			test.WriteLog(Status.FAIL, e);
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}		
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	public void clickAndWait(String locator,String desc) throws IOException {
		try {
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(1000);
			test.WriteLog(Status.PASS, desc);
		}

		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("Click failed", error);
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}		
	}

	/**
	 * 
	 * @param locator  - xpath to element
	 * @param textToType
	 * @param desc
	 * @throws IOException
	 */
	public void type(String locator,String textToType, String desc) throws IOException {
		try {
			driver.findElement(By.xpath(locator)).sendKeys(textToType);
			test.WriteLog(Status.PASS, desc);
		}

		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("Type failed", error);
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}		
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param textToType
	 * @param desc
	 * @throws IOException
	 */
	public void typeAndWait(String locator,String textToType, String desc) throws IOException {
		try {
			driver.findElement(By.xpath(locator)).sendKeys(textToType);
			Thread.sleep(1000);
			test.WriteLog(Status.PASS, desc);
		}

		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("Type failed", error);
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}		
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	public void scrollToView(String locator,String desc) throws IOException {
		try {
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(locator)));
			test.WriteLog(Status.PASS, desc);

			Thread.sleep(1000);
		} 
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("scrollToView failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"scrollToView failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @return
	 * @throws IOException
	 */
	//getText
	public String getText(String locator,String desc) throws IOException{
		try{
			String value= driver.findElement(By.xpath(locator)).getText();
			test.WriteLog(Status.PASS, desc);
			return value;
		} 
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("getText failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"getText failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param attribute
	 * @param desc
	 * @return attribute
	 * @throws IOException
	 */
	//getAttribute
	public String getAttribute(String locator,String attribute, String desc) throws IOException{
		String value=null;
		try{
			value= driver.findElement(By.xpath(locator)).getAttribute(attribute);
			test.WriteLog(Status.PASS, desc);

		} 
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("getAttribute failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"getAttribute failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
		return value;
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @return isDisplayed(true/false)
	 * @throws IOException
	 */
	//isDisplayed
	public boolean isDisplayed(String locator, String desc) throws IOException{
		try{
			boolean isDisplayed = driver.findElement(By.xpath(locator)).isDisplayed();
			test.WriteLog(Status.PASS, desc);
			return isDisplayed;
		} 
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("isDisplayed failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"isDisplayed failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param expected
	 * @param actual
	 * @param desc
	 * @throws IOException
	 */
	//Assert
	public void assertEquals(Object expected, Object actual, String desc) throws IOException
	{		
		try{
			Assert.assertEquals(expected, actual);
			test.WriteLog(Status.PASS,  "<br>"+desc+"</br>"+" - Expected Value : "+expected+" and Actual Value : "+actual);				
		} 
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("assertEquals failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"assertEquals failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param inputCondition
	 * @param desc
	 * @throws IOException
	 */
	public void assertTrue(Boolean inputCondition, String desc) throws IOException
	{		
		try{
			Assert.assertTrue(inputCondition);
			test.WriteLog(Status.PASS, "<br>"+desc+"</br>"+" Expected :  true and Actual : "+inputCondition);			
		} 
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("assertTrue failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"assertTrue failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param inputCondition
	 * @param desc
	 * @throws IOException
	 */
	public void assertFalse(Boolean inputCondition, String desc) throws IOException
	{		
		try{
			Assert.assertFalse(inputCondition);
			test.WriteLog(Status.PASS, "<br>"+desc+"</br>"+" Expected :  false and Actual : "+inputCondition);			
		} 
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("assertFalse failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"assertFalse failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param fullText
	 * @param expectedText
	 * @param desc
	 * @throws IOException
	 */
	public void assertContains(String fullText, String expectedText, String desc) throws IOException
	{	
		if (fullText.contains(expectedText))
			test.WriteLog(Status.PASS, "<br>"+desc+"</br>"+ "The given text contains the expected text. Full Text : "+fullText+" . Expected Containg Text : "+expectedText);		
		else {
			String error = captureScreenshot();
			test.WriteInfo("assertContains failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"assertContains failed");
			test.WriteLog(Status.PASS, "<br>"+desc+"</br>"+ "The given text doesn not contain the expected text. Full Text : "+fullText+" . Expected Containg Text : "+expectedText);	
			throw new WebDriverException("assertContains - "+desc+" - Full Text : "+fullText+". Expected Contaning Text : "+expectedText);
		}
	}


	/**
	 * 
	 * @param timeinSeconds
	 * @param locator
	 * @param desc
	 * @throws IOException
	 */
	//waitForElement
	public void waitForElement(int timeinSeconds,String locator, String desc) throws IOException{
		try{
			WebDriverWait wait=new WebDriverWait(driver, timeinSeconds);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
			test.WriteLog(Status.PASS, desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("waitForElement failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"waitForElement failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * Close current instance of browser
	 * @param desc
	 * @throws IOException
	 */
	//close, goBack,refresh
	public void close(String desc) throws IOException {
		try {
			driver.close();
			test.WriteLog(Status.PASS, desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("close failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"close failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}		
	}
	/**
	 * 
	 * @param desc
	 * @throws IOException
	 */
	public void goBack(String desc) throws IOException {
		try {
			driver.navigate().back();
			test.WriteLog(Status.PASS, desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("goBack failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"goBack failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}		
	}

	/**
	 * 
	 * @param desc
	 * @throws IOException
	 */
	public void refresh(String desc) throws IOException {
		try {
			driver.navigate().refresh();
			test.WriteLog(Status.PASS, desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("refresh failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"refresh failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}		
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	//clear
	public void clear(String locator, String desc) throws IOException {		
		try{
			driver.findElement(By.xpath(locator)).clear();
			test.WriteLog(Status.PASS, desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("clear failed", error);
			//	test.log(LogStatus.INFO,test.addScreenCapture(error)+"clear failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param textToType
	 * @param desc
	 * @throws IOException
	 */
	//clearAndType
	public void clearAndType(String locator, String textToType, String desc) throws IOException {		
		try{
			clear(locator, desc);
			type(locator, textToType, desc);
			test.WriteLog(Status.PASS, desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("clearAndType failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"clearAndType failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param textToType
	 * @param desc
	 * @throws IOException
	 */
	//clearAndTypeAndWait
	public void clearAndTypeAndWait(String locator, String textToType, String desc) throws IOException {		
		try{
			clear(locator, desc);
			type(locator, textToType, desc);
			Thread.sleep(2000);
			test.WriteLog(Status.PASS, desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("clearAndTypeAndWait failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"clearAndTypeAndWait failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param frameLocator - xpath to frame
	 * @param stepDescription
	 * @throws IOException
	 */
	//selectFrame
	public void selectFrame(String frameLocator, String stepDescription) throws IOException {	
		try {
			driver.switchTo().frame(frameLocator);
			test.WriteLog(Status.PASS, stepDescription+" for "+frameLocator);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("clearAndTypeAndWait failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"clearAndTypeAndWait failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param desc
	 */
	//switchToParentFrame
	public void switchToParentFrame(String desc) {
		driver.switchTo().defaultContent();
		test.WriteLog(Status.PASS, desc);
	}

	/**
	 * 
	 * @param desc
	 * @return ALert text
	 * @throws IOException
	 */
	//getAlert, accept, cancel
	public String getAlert(String desc) throws IOException {
		try {
			String value = driver.switchTo().alert().getText();
			driver.switchTo().alert().accept();	
			test.WriteLog(Status.PASS, desc);
			return value;
		}

		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("getAlert failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"getAlert failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	/**
	 * 
	 * @param stepDescription
	 * @throws IOException
	 */
	public void acceptAlert(String stepDescription) throws IOException{	
		try{
			driver.switchTo().alert().accept();
			test.WriteLog(Status.PASS, stepDescription);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("acceptAlert failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"acceptAlert failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param stepDescription
	 * @throws IOException
	 */
	public void cancelAlert(String stepDescription) throws IOException{	
		try{
			driver.switchTo().alert().dismiss();
			test.WriteLog(Status.PASS, stepDescription);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("cancelAlert failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"cancelAlert failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param stepDescription
	 * @throws IOException
	 */
	//JSclick
	public void JSclick(String locator,String stepDescription) throws IOException{
		try{
			JavascriptExecutor jse = (JavascriptExecutor)driver;            
			jse.executeScript("arguments[0].click();", driver.findElement(By.xpath(locator)));
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("JSclick failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"JSclick failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param stepDescription
	 * @throws IOException
	 */
	//JSclickAndWait
	public void JSclickAndWait(String locator,String stepDescription) throws IOException{
		try{
			JavascriptExecutor jse = (JavascriptExecutor)driver;            
			jse.executeScript("arguments[0].click();", driver.findElement(By.xpath(locator)));
			Thread.sleep(2000);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("JSclickAndWait failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"JSclickAndWait failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	//tabout
	public void tabout(String locator,String desc) throws IOException 
	{
		try {
			driver.findElement(By.xpath(locator)).sendKeys(Keys.TAB);
			test.WriteLog(Status.PASS, "Successfully tabbed out of the control "+desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("tabout failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"tabout failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	//taboutAndWait
	public void taboutAndWait(String locator,String desc) throws IOException 
	{
		try {
			driver.findElement(By.xpath(locator)).sendKeys(Keys.TAB);
			test.WriteLog(Status.PASS, "Successfully tabbed out of the control "+desc);
			Thread.sleep(2000);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("taboutAndWait failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"taboutAndWait failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param keyName - key on keyboard
	 * @param key
	 * @throws IOException
	 */
	//pressKey
	public void pressKey(String locator,Keys keyName,String...key) throws IOException
	{
		try {		
			driver.findElement(By.xpath(locator)).sendKeys(keyName);	
			test.WriteLog(Status.PASS, "Successfully pressed key "+key[0]);
		}

		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("pressKey failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"pressKey failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param keyNames - keys on keyboard
	 * @param keys
	 * @throws IOException
	 */
	//pressKeys
	public void pressKeys(String locator,String keyNames,String...keys) throws IOException
	{
		try {		
			driver.findElement(By.xpath(locator)).sendKeys(keyNames);	
			test.WriteLog(Status.PASS, "Successfully pressed keys "+keys);
		}

		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("pressKeys failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"pressKeys failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @return number of elements matching the xpath
	 * @throws IOException
	 */
	//getXpathCount
	public java.lang.Number getXpathCount(String locator,String desc) throws IOException{
		try{
			List<WebElement> xpathCount = driver.findElements(By.xpath(locator));
			java.lang.Number value = xpathCount.size();
			test.WriteLog(Status.PASS, desc);
			return value;
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("getXpathCount failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"getXpathCount failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	//check
	public void check(String locator,String desc) throws IOException {	
		try{
			WebElement element = driver.findElement(By.xpath(locator));
			if (!element.isSelected()) {
				element.click();
			}
			test.WriteLog(Status.PASS, desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("check failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"check failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	//uncheck
	public void uncheck(String locator,String desc) throws IOException {	
		try{
			WebElement element = driver.findElement(By.xpath(locator));
			if (element.isSelected()) {
				element.click();
			}
			test.WriteLog(Status.PASS, desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("uncheck failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"uncheck failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @return isChecked(true/false)
	 * @throws IOException
	 */
	//isChecked
	public boolean isChecked(String locator,String desc) throws IOException {

		boolean value = false;
		try{
			value=driver.findElement(By.xpath(locator)).isSelected();
			test.WriteLog(Status.PASS, desc);
			return value;
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("uncheck failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"uncheck failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param label
	 * @param desc
	 * @throws IOException
	 */
	//selectByValue, selecyByVisibleText, selectByIndex
	public void selecyByVisibleText(String locator, String label, String desc) throws IOException {
		try{

			Select droplist = new Select(driver.findElement(By.xpath(locator)));
			droplist.selectByVisibleText(label);			
			test.WriteLog(Status.PASS, desc+locator+label);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("selecyByVisibleText failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"selecyByVisibleText failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param label
	 * @param desc
	 * @throws IOException
	 */
	public void selectByValue(String locator, String label, String desc) throws IOException {
		try{

			Select droplist = new Select(driver.findElement(By.xpath(locator)));
			droplist.selectByValue(label);			
			test.WriteLog(Status.PASS, desc+locator+label);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("selectByValue failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"selectByValue failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator
	 * @param index
	 * @param desc
	 * @throws IOException
	 */
	public void selectByIndex(String locator, int index, String desc) throws IOException {
		try{

			Select droplist = new Select(driver.findElement(By.xpath(locator)));
			droplist.selectByIndex(index);			
			test.WriteLog(Status.PASS, desc+locator+index);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("selectByIndex failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"selectByIndex failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	//moveTo, rightClick, doubleClick
	public void moveToElement(String locator,String desc) throws IOException
	{
		try {
			Actions action = new Actions(driver);
			WebElement we = driver.findElement(By.xpath(locator));
			action.moveToElement(we).build().perform();
			test.WriteLog(Status.PASS, "Successfully moved to element "+desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("moveToElement failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"moveToElement failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	public void rightClick(String locator,String desc) throws IOException
	{
		try {
			Actions action = new Actions(driver);
			WebElement we = driver.findElement(By.xpath(locator));
			action.contextClick(we).build().perform();
			test.WriteLog(Status.PASS, "rightClick "+desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("rightClick failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"rightClick failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	public void doubleClick(String locator,String desc) throws IOException
	{
		try {
			Actions action = new Actions(driver);
			WebElement we = driver.findElement(By.xpath(locator));
			action.doubleClick(we).build().perform();
			test.WriteLog(Status.PASS, "doubleClick "+desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("doubleClick failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"doubleClick failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	/**
	 * 
	 * @param driver
	 * @return
	 * @throws IOException
	 */
	//captureScreenshotScreenshot
	public String captureScreenshot() throws IOException {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		f=new File(f.toString()+File.separator+"Screenshots");
		f.mkdir();

		File Dest = new File(f + File.separator + System.currentTimeMillis()+ ".png");
		String errflpath = Dest.getAbsolutePath();
		Files.copy(scrFile, Dest);
		System.out.println(errflpath);
		return errflpath;
	}

	public int generateRandomNumber(int upperBound) {
		Random rand = new Random();
		int int_random = rand.nextInt(upperBound); 

		System.out.println("Random integer value from 0 to" + (upperBound-1) + " : "+ int_random);

		return int_random;
	}

	public String encryptPwd(String text) throws Exception {
		String key="Bar12345Bar12345";
		Key aesKey=new SecretKeySpec(key.getBytes(),"AES");
		Cipher cipher=Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, aesKey);
		byte[] encrypted=cipher.doFinal(text.getBytes());
		Base64.Encoder encoder=Base64.getEncoder();
		String encryptedString=encoder.encodeToString(encrypted);
		System.out.println(encryptedString);
		return encryptedString;
		
	}
	
	public String decryptPwd(String encryptedString) throws Exception{
		String key="Bar12345Bar12345";
		Key aesKey=new SecretKeySpec(key.getBytes(),"AES");
		Cipher cipher=Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, aesKey);
		Base64.Decoder decoder=Base64.getDecoder();
		cipher.init(Cipher.DECRYPT_MODE, aesKey);
		String decrypted=new String(cipher.doFinal(decoder.decode(encryptedString)));
		return decrypted;
	}

	@AfterMethod
	public void closeBrowser() {
		driver.close();
	}

	@AfterSuite
	public void extentFlush() {
		/**if (report != null) {
			report.endTest(test);
			report.flush();
		} **/


		test.flushlog();

		if (driver != null) {
			driver.quit();
		}		
	}


}
