package common.mobile;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.remote.DesiredCapabilities;

public class MobileCaps {

	public Map<String, String> capabilitiesMap = new HashMap<String, String>();
	public DesiredCapabilities capabilities = new DesiredCapabilities();

	public void AndroidCaps() {
		//Android caps	
		capabilitiesMap.put("appiumServerPort", "4723");
		capabilitiesMap.put("appium-version", "1.17.1");
		capabilitiesMap.put("newCommandTimeout", "5");
		capabilitiesMap.put("platformVersion", "10.0");
		capabilitiesMap.put("appiumServerIP", "localhost");
		capabilitiesMap.put("browserName", "Chrome");
		capabilitiesMap.put("platformName", "Android");
		capabilitiesMap.put("deviceName", "AutomationAndroid");
		capabilitiesMap.put("nativeWebScreenshot", "true");
		capabilitiesMap.put("chromedriverExecutableDir", 
				System.getProperty("user.dir")+File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator+"mobiledrivers");

	}

	public void iOSCaps() {
		//iOS caps	
		capabilitiesMap.put("appiumServerIP","localhost");
		capabilitiesMap.put("appiumServerPort","4723");
		capabilitiesMap.put("appium-version","1.18.0");
		capabilitiesMap.put("platformName","iOS");
		capabilitiesMap.put("platformVersion","14.0");
		capabilitiesMap.put("deviceName","iPhone 8");
		capabilitiesMap.put("browserName","Safari");
		capabilitiesMap.put("newCommandTimeout","120");		
	}
}
