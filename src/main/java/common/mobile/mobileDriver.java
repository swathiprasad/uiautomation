package common.mobile;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.Status;

import common.web.Driver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import locators.mobile.encash.EncashMobileLocators;
import locators.web.encash.EncashWebLocators;
import utils.ExtentReport;

public class mobileDriver extends Driver
{

	public AndroidDriver androidDriver;
	public IOSDriver iOSDriver;
	public WebDriverWait waitObj;
	public String platformName;
	
	
	@BeforeSuite
	/**
	 * Configure log4j.properties
	 */
	public void configurationRead() {
		PropertyConfigurator.configure(System.getProperty("user.dir")+File.separator+"src/main/resources"+File.separator+"log4j.properties");
		// test = new ExtentReport() ;
		
		PROCONFIG = initProjectVar(EncashWebLocators.projectVariables);

		String date=getCurrentDate();
		String time=getCurrentTime();
		File f=new File(System.getProperty("user.dir")+File.separator+"TestResults"+File.separator+date+"_"+time);
		f.mkdir();
		test = new ExtentReport(f.toString()+File.separator+"ExtentReport.html") ;
		//report = new ExtentReports(f.toString()+File.separator+"ExtentReport.html",true);
	}
		
	@SuppressWarnings("rawtypes")
	/**
	 * 
	 * @param m - current method name
	 * @param testArgs
	 * @throws Exception
	 */
	@BeforeMethod
	public void init(Method m) throws Exception {

		//test=report.startTest(m.getName());			
		test.CreateTest(m.getName());
		MobileCaps mobCaps=new MobileCaps();
		String mobilePlatform=PROCONFIG.getProperty("mobilePlatform");
		if (mobilePlatform.equalsIgnoreCase("Android"))
			mobCaps.AndroidCaps();
		else if(mobilePlatform.equalsIgnoreCase("iOS"))
			mobCaps.iOSCaps();

		//Set capabilities
		for (Map.Entry<String, String> entry : mobCaps.capabilitiesMap.entrySet()) {
			mobCaps.capabilities.setCapability(entry.getKey(), entry.getValue());
		}

		String appiumServerIP=mobCaps.capabilitiesMap.get("appiumServerIP");
		String appiumServerPort=mobCaps.capabilitiesMap.get("appiumServerPort");
		platformName=mobCaps.capabilitiesMap.get("platformName");

		//Browser setup		
		if(platformName.equalsIgnoreCase("Android")) {
			String serverUrl="http://"+ appiumServerIP + ":" + appiumServerPort + "/wd/hub"; System.out.println(serverUrl);
			androidDriver = new AndroidDriver(new URL(serverUrl), mobCaps.capabilities);
			String url=PROCONFIG.getProperty("url");	
			androidDriver.get(url);
			//driver=androidDriver;
			waitObj = new WebDriverWait(androidDriver, 60);
		}
		else if(platformName.equalsIgnoreCase("ios")) {
			iOSDriver = new IOSDriver(new URL("http://" + appiumServerIP + ":" + appiumServerPort + "/wd/hub"), mobCaps.capabilities);
			iOSDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			waitObj = new WebDriverWait(iOSDriver, 30);
			String url=PROCONFIG.getProperty("url");	
			iOSDriver.get(url);
			//driver=iOSDriver;
			waitObj = new WebDriverWait(iOSDriver, 60);
		}		
		
	}	

	

	/**
	 * Logout
	 */
	public void logout() {
		driver.findElement(By.xpath(EncashMobileLocators.encash_mobile_loggedInUser)).click();
		driver.findElement(By.xpath(EncashMobileLocators.encash_mobile_logout)).click();
	}


	//type
	/**
	 * 
	 * @param locator  - xpath to element
	 * @param textToType
	 * @param desc
	 * @throws IOException
	 */
	public void type(String locator, String value,String desc) throws IOException {

		try {
			if (platformName.equalsIgnoreCase("android")) {
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				androidDriver.findElement(By.xpath(locator)).sendKeys(value);
			} else if(platformName.equalsIgnoreCase("ios")) {
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				iOSDriver.findElement(By.xpath(locator)).sendKeys(value);
			}   
			test.WriteLog(Status.PASS, desc+" - "+locator+value);

		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "Type failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"Type failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}	
	}

	//click
	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @throws IOException
	 */
	public void click(String locator,String desc) throws IOException {

		try {

			if (platformName.equalsIgnoreCase("android")) 	{
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));			
				androidDriver.findElement(By.xpath(locator)).click();
			}
			else if (platformName.equalsIgnoreCase("ios"))	{		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				iOSDriver.findElement(By.xpath(locator)).click();
			}

			test.WriteLog(Status.PASS, desc+" - "+locator);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "Click failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"Click failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}	
	}

	//pressKey
	/**
	 * 
	 * @param locator - xpath to element
	 * @param keyName - key on keyboard
	 * @param key
	 * @throws IOException
	 */
	public void pressKey(String locator,Keys keyName,String desc) throws IOException {

		try {
			if (platformName.equalsIgnoreCase("android")) {		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));	
				androidDriver.findElement(By.xpath(locator)).sendKeys(keyName);
			}
			else if (platformName.equalsIgnoreCase("ios"))	{		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));	
				iOSDriver.findElement(By.xpath(locator)).sendKeys(keyName);
			}
			test.WriteLog(Status.PASS, desc+" - "+locator);

		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "Press key failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"Press key failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}	
	}
	//getAttribute
	/**
	 * 
	 * @param locator - xpath to element
	 * @param attribute
	 * @param desc
	 * @return attribute
	 * @throws IOException
	 */
	public String getAttribute(String locator,String attributeName,String desc) throws IOException {

		String attributeValue =null;
		try {
			if (platformName.equalsIgnoreCase("android")) {		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				attributeValue = androidDriver.findElement(By.xpath(locator)).getAttribute(attributeName);
			}
			else if (platformName.equalsIgnoreCase("ios")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				attributeValue = iOSDriver.findElement(By.xpath(locator)).getAttribute(attributeName);
			}

			test.WriteLog(Status.PASS, "Get attribute"+" - "+desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "getAttribute failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"getAttribute failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}	
		return attributeValue;
	}

	//getText
	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @return
	 * @throws IOException
	 */
	public String getText(String locator,String desc) throws IOException {

		String text =null;
		try {
			if (platformName.equalsIgnoreCase("android")) {		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				text = androidDriver.findElement(By.xpath(locator)).getText();
			}
			else if (platformName.equalsIgnoreCase("ios")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				text = iOSDriver.findElement(By.xpath(locator)).getText();
			}
			test.WriteLog(Status.PASS, "Get text"+" - "+desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "getText failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"getText failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}	
		return text;
	}

	//isDisplayed
	/**
	 * 
	 * @param locator - xpath to element
	 * @param desc
	 * @return isDisplayed(true/false)
	 * @throws IOException
	 */
	public boolean isDisplayed(String locator, String desc) throws IOException {

		boolean status=false;
		try {
			if (platformName.equalsIgnoreCase("android")) {		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				status = androidDriver.findElement(By.xpath(locator)).isDisplayed();
			}
			else if (platformName.equalsIgnoreCase("ios")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				status = iOSDriver.findElement(By.xpath(locator)).isDisplayed();
			}
			test.WriteLog(Status.PASS, "Get text"+" - "+desc);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "isDisplayed failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"isDisplayed failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}	
		return status;
	}

	/**
	 * 
	 * @param expected
	 * @param actual
	 * @param desc
	 * @throws IOException
	 */
	//Assert
	public void assertEquals(Object expected, Object actual, String desc) throws IOException
	{		
		try{
			Assert.assertEquals(expected, actual);
			test.WriteLog(Status.PASS,  "<br>"+desc+"</br>"+" - Expected Value : "+expected+" and Actual Value : "+actual);				
		} 
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("assertEquals failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"assertEquals failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param inputCondition
	 * @param desc
	 * @throws IOException
	 */
	public void assertTrue(Boolean inputCondition, String desc) throws IOException
	{		
		try{
			Assert.assertTrue(inputCondition);
			test.WriteLog(Status.PASS, "<br>"+desc+"</br>"+" Expected :  true and Actual : "+inputCondition);			
		} 
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("assertTrue failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"assertTrue failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param inputCondition
	 * @param desc
	 * @throws IOException
	 */
	public void assertFalse(Boolean inputCondition, String desc) throws IOException
	{		
		try{
			Assert.assertFalse(inputCondition);
			test.WriteLog(Status.PASS, "<br>"+desc+"</br>"+" Expected :  false and Actual : "+inputCondition);			
		} 
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteInfo("assertFalse failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"assertFalse failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	/**
	 * 
	 * @param fullText
	 * @param expectedText
	 * @param desc
	 * @throws IOException
	 */
	public void assertContains(String fullText, String expectedText, String desc) throws IOException
	{	
		if (fullText.contains(expectedText))
			test.WriteLog(Status.PASS, "<br>"+desc+"</br>"+ "The given text contains the expected text. Full Text : "+fullText+" . Expected Containg Text : "+expectedText);		
		else {
			String error = captureScreenshot();
			test.WriteInfo("assertContains failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"assertContains failed");
			test.WriteLog(Status.PASS, "<br>"+desc+"</br>"+ "The given text doesn not contain the expected text. Full Text : "+fullText+" . Expected Containg Text : "+expectedText);	
			throw new WebDriverException("assertContains - "+desc+" - Full Text : "+fullText+". Expected Contaning Text : "+expectedText);
		}
	}
	
	//scrollToWebElement(String locator)
	/**
	 * 
	 * @param locator - xpath to element
	 * @throws IOException
	 */
	public void scrollToWebElement(String locator) throws IOException {

		try {

			if (platformName.equalsIgnoreCase("android")){	
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				((JavascriptExecutor)androidDriver).executeScript("arguments[0].scrollIntoView(true);", androidDriver.findElement(By.xpath((locator))));
			}
			else if (platformName.equalsIgnoreCase("ios")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				((JavascriptExecutor)iOSDriver).executeScript("arguments[0].scrollIntoView(true);", iOSDriver.findElement(By.xpath((locator))));
			}
			test.WriteLog(Status.PASS, "scrollToWebElement"+" - "+locator);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "scrollToWebElement failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"scrollToWebElement failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	//	//sendKeyEvent(AndroidKey)
	//	public void sendKeyEvent(AndroidKey key) throws IOException {
	//		try {
	//			if (platformName.equalsIgnoreCase("android")) 
	//				androidDriver.pressKey(new KeyEvent().withKey(key) );
	//
	//			test.WriteLog(Status.PASS, "Send a key event to the device Key - "+key.toString());
	//		}
	//		catch(Exception | Error e) {
	//			String error = captureScreenshot();
	//			test.log(LogStatus.INFO,test.addScreenCapture(error)+"sendKeyEvent failed");
	//			test.WriteLog(Status.FAIL, e);
	//			throw new WebDriverException(e);
	//		}
	//	}

	//getOrientation()
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public org.openqa.selenium.ScreenOrientation getOrientation() throws IOException
	{
		org.openqa.selenium.ScreenOrientation screenOrientation=null;
		try {
			if (platformName.equalsIgnoreCase("android")) 
				screenOrientation = androidDriver.getOrientation();

			else if (platformName.equalsIgnoreCase("ios"))
				screenOrientation = iOSDriver.getOrientation();

			test.WriteLog(Status.PASS, "Get the Orientation "+screenOrientation.toString());
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "getOrientation failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"getOrientation failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
		return screenOrientation;
	}


	//hideKeyboard()
	/**
	 * 
	 * @throws IOException
	 */

	public void hideKeyboard() throws IOException
	{
		try {
			if (platformName.equalsIgnoreCase("android")) 
				androidDriver.hideKeyboard();
			else if (platformName.equalsIgnoreCase("ios"))
				iOSDriver.hideKeyboard();

			test.WriteLog(Status.PASS, "Hide Keyboard");
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "Hide Keyboard failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"Hide Keyboard failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	//pinch(int x, int y)
	/**
	 * 
	 * @param x
	 * @param y
	 * @throws IOException
	 */
	public void pinch(int x, int y) throws IOException
	{    	  	
		try {
			if (platformName.equalsIgnoreCase("android")) 
			{
				int scrHeight = androidDriver.manage().window().getSize().getHeight(); // To get the mobile screen height
				int scrWidth = androidDriver.manage().window().getSize().getWidth();// To get the mobile screen width
				MultiTouchAction multiTouch = new MultiTouchAction((MobileDriver)androidDriver);
				TouchAction touchAction4 = new TouchAction((MobileDriver)androidDriver);
				TouchAction touchAction3 = new TouchAction((MobileDriver)androidDriver);
				touchAction3.press(new PointOption().withCoordinates(100,100)).waitAction(new WaitOptions().waitOptions(Duration.ofSeconds(1)))
				.moveTo(new PointOption().withCoordinates(scrWidth / 2, scrHeight / 2)).release();
				touchAction4.press(new PointOption().withCoordinates(scrWidth - 100, scrHeight - 100)).waitAction(new WaitOptions().waitOptions(Duration.ofSeconds(1)))
				.moveTo(new PointOption().withCoordinates(scrWidth / 2, scrHeight / 2)).release();
				multiTouch.add(touchAction3).add(touchAction4);
				multiTouch.perform();
			}else if (platformName.equalsIgnoreCase("ios")){
				int scrHeight = iOSDriver.manage().window().getSize().getHeight(); // To get the mobile screen height
				int scrWidth = iOSDriver.manage().window().getSize().getWidth();// To get the mobile screen width
				MultiTouchAction multiTouch = new MultiTouchAction((MobileDriver)iOSDriver);
				TouchAction touchAction4 = new TouchAction((MobileDriver)iOSDriver);
				TouchAction touchAction3 = new TouchAction((MobileDriver)iOSDriver);
				touchAction3.press(new PointOption().withCoordinates(100,100)).waitAction(new WaitOptions().waitOptions(Duration.ofSeconds(1)))
				.moveTo(new PointOption().withCoordinates(scrWidth / 2, scrHeight / 2)).release();
				touchAction4.press(new PointOption().withCoordinates(scrWidth - 100, scrHeight - 100)).waitAction(new WaitOptions().waitOptions(Duration.ofSeconds(1)))
				.moveTo(new PointOption().withCoordinates(scrWidth / 2, scrHeight / 2)).release();
				multiTouch.add(touchAction3).add(touchAction4);
				multiTouch.perform();

			}
			test.WriteLog(Status.PASS, "Pinch an element on the screen using x and y ."+ "x="+x+" y"+y);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "pinch failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"pinch failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}


	//rotate(org.openqa.selenium.ScreenOrientation orientation)
	/**
	 * 
	 * @param orientation
	 * @throws IOException
	 */
	public void rotate(org.openqa.selenium.ScreenOrientation orientation) throws IOException
	{
		try {
			if (platformName.equalsIgnoreCase("android")) 
				androidDriver.rotate(orientation);

			else if (platformName.equalsIgnoreCase("ios"))
				iOSDriver.rotate(orientation);			

			test.WriteLog(Status.PASS, "Rotate Orientation "+ orientation.toString());
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "rotate failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"rotate failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	//swipe(int startx, int starty, int endx, int endy, int duration)
	/**
	 * 
	 * @param startx
	 * @param starty
	 * @param endx
	 * @param endy
	 * @param duration
	 * @throws IOException
	 */
	public void swipe(int startx, int starty, int endx, int endy, int duration) throws IOException
	{
		try {
			if (platformName.equalsIgnoreCase("android")) {
				(new TouchAction((MobileDriver)androidDriver))
				.press(PointOption.point(startx, starty))
				.moveTo(PointOption.point(endx, endy))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
				.release()
				.perform();

			}else if (platformName.equalsIgnoreCase("ios")){
				(new TouchAction((MobileDriver)iOSDriver))
				.press(PointOption.point(startx, starty))
				.moveTo(PointOption.point(endx, endy))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
				.release()
				.perform();
			}	
			test.WriteLog(Status.PASS, "Swiping across the screen", " "+"startx "+startx+" starty "+starty+" endx "+endx +" endy "+ endy + " duration "+duration );
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "swipe failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"swipe failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}
	}

	//swipeFromBottomToUp() - Code to Swipe DOWN
	/**
	 * 
	 * @throws IOException
	 */
	public void swipeFromBottomToUp() throws IOException
	{
		try {
			JavascriptExecutor js = (JavascriptExecutor) androidDriver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", "down");
			js.executeScript("mobile: scroll", scrollObject);
			System.out.println("Swipe down was Successfully done");
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "swipeFromBottomToUp failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"swipeFromBottomToUp failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	//swipeFromUpToBottom() - Code to Swipe UP
	/**
	 * 
	 * @throws IOException
	 */
	public void swipeFromUpToBottom() throws IOException
	{
		try {

			JavascriptExecutor js = (JavascriptExecutor) androidDriver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", "up");
			js.executeScript("mobile: scroll", scrollObject);
			System.out.println("Swipe up was Successfully done.");
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "swipeFromUpToBottom failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"swipeFromUpToBottom failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	//tap(int fingers, String locator, int duration)
	/**
	 * 
	 * @param fingers
	 * @param locator
	 * @param duration
	 * @throws IOException
	 */
	public void tap(int fingers, String locator, int duration) throws IOException
	{    	
		MobileElement element;
		ElementOption eoption;
		try {    		
			if (platformName.equalsIgnoreCase("android")) {		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				element = (MobileElement) androidDriver.findElement(By.xpath(locator));
				eoption = new ElementOption().withElement(element);
				//androidDriver.tap(fingers, element, duration);
				TouchAction tapAction = new TouchAction((MobileDriver)androidDriver).tap(new TapOptions().withElement(eoption));
				tapAction.perform();

				TouchAction tapAction1 = new TouchAction((MobileDriver)androidDriver).tap(new TapOptions().withElement(eoption));
				tapAction.perform();
			}else if (platformName.equalsIgnoreCase("ios")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				element = (MobileElement) iOSDriver.findElement(By.xpath(locator));
				eoption = new ElementOption().withElement(element);
				//iOSDriver.tap(fingers, element, duration);
				TouchAction tapAction = new TouchAction((MobileDriver)iOSDriver).tap(new TapOptions().withElement(eoption));
				tapAction.perform();
			}

			//GlobalFunctions.LogMessage("tap", "Tapping the center of an element on the screen", "","fingers "+fingers+"locator "+locator+" duration "+duration , "", "Done");
			test.WriteLog(Status.PASS, "Tapping the center of an element on the screen "+"fingers "+fingers+"locator "+locator+" duration "+duration);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "tap failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"tap failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	//webSelectByVisibleText(String locator,String text)
	/**
	 * 
	 * @param locator - xpath of element
	 * @param text
	 * @throws IOException
	 */
	public void webSelectByVisibleText(String locator,String text) throws IOException
	{

		try {

			if (platformName.equalsIgnoreCase("android")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				new Select(androidDriver.findElement(By.xpath(locator))).selectByVisibleText(text);
			}
			else if (platformName.equalsIgnoreCase("ios")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				new Select(androidDriver.findElement(By.xpath(locator))).selectByVisibleText(text);
			}

			test.WriteLog(Status.PASS, "WebSelect By Visible Text "+ locator);
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "webSelectByVisibleText failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"webSelectByVisibleText failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	//webSelectByValue(String locator,String value)
	/**
	 * 
	 * @param locator - xpath of element
	 * @param value
	 * @throws IOException
	 */
	public void webSelectByValue(String locator,String value) throws IOException
	{


		try {

			if (platformName.equalsIgnoreCase("android")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				new Select(androidDriver.findElement(By.xpath(locator))).selectByValue(value);
			}
			else if (platformName.equalsIgnoreCase("ios")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				new Select(androidDriver.findElement(By.xpath(locator))).selectByValue(value);
			}

			test.WriteLog(Status.PASS, "WebSelect By Value");
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "webSelectByValue failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"webSelectByValue failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}
	//webSelectByIndex(String locator,int index)
	/**
	 * 
	 * @param locator - xpath of element
	 * @param index
	 * @throws IOException
	 */
	public void webSelectByIndex(String locator,int index) throws IOException
	{


		try {

			if (platformName.equalsIgnoreCase("android")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				new Select(androidDriver.findElement(By.xpath(locator))).selectByIndex(index);
			}
			else if (platformName.equalsIgnoreCase("ios")){		
				waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				new Select(androidDriver.findElement(By.xpath(locator))).selectByIndex(index);
			}

			test.WriteLog(Status.PASS, "WebSelect By Index");
		}
		catch(Exception | Error e) {
			String error = captureScreenshot();
			test.WriteLog(Status.INFO, "webSelectByIndex failed", error);
			//test.log(LogStatus.INFO,test.addScreenCapture(error)+"webSelectByIndex failed");
			test.WriteLog(Status.FAIL, e);
			throw new WebDriverException(e);
		}

	}

	@AfterMethod
	public void closeBrowser() {
		if(platformName.equalsIgnoreCase("Android")) 
			androidDriver.close();
		else if(platformName.equalsIgnoreCase("ios")) 
			iOSDriver.close();
	}

	@AfterSuite
	public void extentFlush() {
		/**if (report != null) {
			report.endTest(test);
			report.flush();
		}	**/
		test.flushlog();

	}
}
