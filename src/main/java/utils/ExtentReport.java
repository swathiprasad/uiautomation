package utils;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import common.web.Driver;

public class ExtentReport extends Driver{

	public ExtentHtmlReporter htmlreporter;	
	public ExtentReports extent;
	public ExtentTest extenttest;

	public ExtentReport(String filelocation)  {

		String date=getCurrentDate();
		String time=getCurrentTime();

//		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
//		String browserName = cap.getBrowserName().toLowerCase();
//	    String os = cap.getPlatform().toString();
//	    String v = cap.getVersion().toString();
	    
		this.htmlreporter = new ExtentHtmlReporter(filelocation);
		this.htmlreporter.loadXMLConfig(System.getProperty("user.dir")+File.separator+"src/main/resources"+File.separator+"extentreportproperties.xml");
		//this.spark = new ExtentSparkReporter(ConstantVariable.ExtentReportsLocation) ;
		//this.spark.loadXMLConfig(ConstantVariable.ExtentReportsPropeties);
		this.extent = new ExtentReports();
		this.extent.setSystemInfo("Organization", "Enchash-Offers");
		//this.extent.setSystemInfo("Browser", browserName+ " - "+os+ " - "+v);
		this.extent.setSystemInfo("Operation System ", System.getProperty("os.name"));
		this.extent.setSystemInfo("OS Version number", System.getProperty("os.version"));
		this.extent.setSystemInfo("Build Version No", "Need to fetch the Build Number");
		this.extent.attachReporter(this.htmlreporter);
		
		//driver.close();
	}

	public void CreateTest(String TestName) {
		this.extenttest = this.extent.createTest(TestName);
		//this.extent.setAnalysisStrategy(strategy);		
	}

	public void CreateTest(String TestName, String Description) {
		this.extenttest = this.extent.createTest(TestName, Description);
	}

	public void WriteLog(Status status, String details) {
		this.extenttest.log(status, details);
	}

	public void flushlog() {
		this.extent.flush();
	}

	public void AttachScreenshot(String imagePath) throws IOException {
		this.extenttest.addScreenCaptureFromPath(imagePath);
	}

	public void WriteInfo(String details) {
		this.extenttest.info(details);		
	}

	public void WriteInfo(String details, String ImageFileLocation) throws IOException {
		this.extenttest.info(details, MediaEntityBuilder.createScreenCaptureFromPath(ImageFileLocation).build());		
	}

	public void WriteLog(Status status,String details, String ImageFilelocation) throws IOException {
		this.extenttest.log(status, details, MediaEntityBuilder.createScreenCaptureFromPath(ImageFilelocation).build());
	}

	public void WriteLog(Status status, Throwable t) {
		this.extenttest.log(status,t);		
	}

	public void Categeory(String category) {
		this.extenttest.assignCategory(category);
	}

	public void author(String author) {
		this.extenttest.assignAuthor(author);		
	}
}